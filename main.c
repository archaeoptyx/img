#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"
#include <string.h>

int main(int argc, char** argv){
  struct image matrice;
  //f permet de vérifier si il y a une image en entrée/sortie
  int i,f,o;
  char* sortie;
  f=0;
  o=0;
  //on fait une boucle pour permettre pleins de commande et dans un ordre quelconque
  for(i=0;i<argc-1;i++){
    if(strcmp(argv[1],"-h")==0){
      printf("- Aide - \n");
      printf("./img -h : affiche l'aide\n");
      printf("./img -i fichier.ppm -g -o nouvelleimage.ppm : transforme votre image en gris\n");
      printf("./img -i fichier.ppm -b seuil -o nouvelleimage.ppm : binarise votre image\n");
      printf("./img -i fichier.ppm -c -o nouvelleimage.ppm : renforce le contraste\n");
      printf("./img -i fichier.ppm -n -o nouvelleimage.ppm : négatif de l'image\n");
      printf("./img -i fichier.ppm -m -o nouvelleimage.ppm : miroir de l'image\n");
      printf("./img -i fichier.ppm -p -o nouvelleimage.ppm : rotation de l'image\n");
      printf("./img -i fichier.ppm -d -o nouvelleimage.ppm : dilatation de l'image\n");
      printf("./img -i fichier.ppm -e -o nouvelleimage.ppm : réalise une érosion\n");
      printf("./img -i fichier.ppm -f -o nouvelleimage.ppm : réalise un flou\n");
      printf("./img -i fichier.ppm -l -o nouvelleimage.ppm : détection de contours\n");
      printf("./img -i fichier.ppm -r -o nouvelleimage.ppm : recadrage dynamique\n");
      //printf("./img -i fichier.ppm -Z -o nouvelleimage.ppm : réalise un zoom *2\n");
      //printf("./img -i fichier.ppm -z -o nouvelleimage.ppm : réalise un dézoom *2\n");
      //printf("./img -i fichier.ppm -s -o nouvelleimage.ppm : réalise une segmentation\n");
      printf("./img -x nouvelleimage.ppm hauteur largeur épaisseur : crée une croix\n");
      return 0;
    }if((strcmp(argv[i],"-i")==0)){
      //on met directement l'image en entrée
      matrice = Chargement(argv[i+1]);
      f=1;
    }if((strcmp(argv[i],"-o")==0)){
      //pour avoir l'image en sortie
      sortie = argv[i+1];
      o=1;
    }
  }
    if((f==1)&&(o==1)){
      for(i=0;i<argc;i++){
      if((strcmp(argv[i],"-g")==0)&&(f==1)&&(o==1)){
        gris(matrice);
      }if((strcmp(argv[i],"-b")==0)&&(f==1)&&(o==1)){
        if((atoi(argv[i+1]))!=0){
          //la condition permet de vérifier si le seuil est un nombre
          binarisation(matrice,atoi(argv[i+1]));
        }else{
          //message d'erreur pour le seuil
          printf("Le seuil n'est pas correctement écrit\n");
        }
      }if((strcmp(argv[i],"-m")==0)&&(f==1)&&(o==1)){
        miroir(matrice);
      }if((strcmp(argv[i],"-p")==0)&&(f==1)&&(o==1)){
        rotation(matrice);
      }if((strcmp(argv[i],"-n")==0)&&(f==1)&&(o==1)){
        negatif(matrice);
      }if((strcmp(argv[i],"-r")==0)&&(f==1)&&(o==1)){
        recadragedynamique(matrice);
      }if((strcmp(argv[i],"-c")==0)&&(f==1)&&(o==1)){
        contraste(matrice);
      }if((strcmp(argv[i],"-f")==0)&&(f==1)&&(o==1)){
        floutage(matrice);
      }if((strcmp(argv[i],"-l")==0)&&(f==1)&&(o==1)){
        contour(matrice);
      }if((strcmp(argv[i],"-e")==0)&&(f==1)&&(o==1)){
        creation("croix.ppm",5,5,1);
        struct image croix;
        croix = Chargement("croix.ppm");
        croixblanche(croix);
        erosion(matrice, croix);
      }if((strcmp(argv[i],"-d")==0)&&(f==1)&&(o==1)){
        creation("croix.ppm",5,5,1);
        struct image croix;
        croix = Chargement("croix.ppm");
        croixblanche(croix);
        dilatation(matrice, croix);
      }
      //on écrit l'image après avoir modifié la matrice
    }
    return sauvegarde(matrice,sortie);
  }else if(o==1){
    if((strcmp(argv[1],"-x")==0)&&(o==1)){
      creation(sortie,atoi(argv[4]),atoi(argv[5]),atoi(argv[6]));
  }else{
    //message d'erreur ni entrée ni sortie ni bonne commande
    printf("Il y a un problème avec l'image d'entrée ou de sortie\n");
  }
}else{
  //message d'erreur ni entrée ni sortie ni bonne commande
  printf("Il y a un problème avec l'image d'entrée ou de sortie\n");
}
}
