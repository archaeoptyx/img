﻿

Description du projet: Ce projet permet le traitement d’image en format PPM,

notamment

la transformer en gris, réaliser un effet de miroir, effectuer une rotation,

etc.

Statut du projet: Terminé.

Instructions:

Afin d'exécuter le programme, il est nécessaire d’utiliser unix pour compiler et

exécuter.

Créez un répertoire pour y placer tous les fichiers du projet qui sont présents

dans la

branche main du Gitlab.

Utilisez la commande cd <Nom du répertoire> dans le terminal pour vous déplacer

dans le

répertoire.

Tapez “make” pour compiler le programme.

-Si vous voulez afficher une aide pour utiliser le programme et connaître toutes

les options

possibles, tapez : ./img -h.

-Si vous voulez effectuer une transformation d’image en gris, tapez:

./img -i <image.ppm> -g -o <nouvelleimage.ppm>

-Si vous voulez binariser votre image à un certain seuil, tapez:

./img -i <image.ppm> -b seuil -o <nouvelleimage.ppm>

-Si vous voulez renforcer le contraste de votre image, tapez:

./img -i <image.ppm> -c -o <nouvelleimage.ppm>

-Si vous voulez obtenir le négatif de votre image, tapez:

./img -i <image.ppm> -n -o <nouvelleimage.ppm>

-Si vous voulez obtenir le miroir de votre image, tapez:

./img -i <image.ppm> -m -o <nouvelleimage.ppm>

-Si vous voulez obtenir la rotation de votre image à 90° dans le sens horaire,

tapez:

./img -i <image.ppm> -p -o <nouvelleimage.ppm>

-Si vous voulez créer un effet de dilatation de l’image, tapez:

./img -i <image.ppm> -d -o <nouvelleimage.ppm>

-Si vous voulez créer un effet d’érosion de l’image, tapez:

./img -i <image.ppm> -e -o <nouvelleimage.ppm>

-Si vous voulez flouter votre image, tapez:

./img -i <image.ppm> -f -o <nouvelleimage.ppm>

-Si vous voulez mettre en évidence les contours de votre image, tapez:

./img -i <image.ppm> - l -o <nouvelleimage.ppm>

-Si vous voulez réaliser un recadrage dynamique, tapez:

./img -i <image.ppm> -r -o <nouvelleimage.ppm>

-Si vous voulez créer une croix avec une certaine hauteur, largeur et épaisseur,

tapez:

./img -x -o <croix.ppm> hauteur largeur épaisseur

Il est possible d’inverser les fichiers d’entrée et de sortie, par exemple, pour

transformer

votre image en gris, vous pouvez taper:

./img -o <nouvelleimage.ppm> -g -i <image.ppm>

Il est aussi possible de combiner plusieurs effets, par exemple, si vous voulez

un contraste

de votre image en négatif, vous pouvez taper:

./img -i <image.ppm> -n -c -o <nouvelleimage.ppm>


