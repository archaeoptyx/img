#include <stdio.h>
#include <stdlib.h>
#include "fonctions.h"
#define N 10

// Auteur :  Lisa
// Date :  24/05
// Modifications : Khadija
// Résumé :  Fonction qui permet de récolter les informations d'une image tel que les couleurs des pixels et sa taille
// Entrée(s) :  La chaîne de caractère liée au nom de l'image
// Sortie(s) :  Variable structure image
struct image Chargement(char* bender){
  FILE *fichier = NULL;
  char type[N];
  int i,j,max;
  RVB pixel;
  image matrice;
//ouvrir le fichier et quitter le code si raté
  if((fichier=fopen(bender,"r"))==NULL){
    printf("Impossible de trouver le fichier \n");
    exit(1);
  }

//Pour stocker les trois premières valeurs du fichier ppm
  fscanf(fichier, "%s", type);
  fscanf(fichier, "%d", &matrice.nbligne);
  fscanf(fichier, "%d", &matrice.nbcolonne);
  fscanf(fichier, "%d", &matrice.nbcouleur);


//allocation tableau dynamique 2D pour stocker les valeurs RVB pixels
  matrice.tabpixel=malloc(matrice.nbligne * sizeof (RVB*));
  for (i=0;i<matrice.nbligne;i++){
    matrice.tabpixel[i]=malloc(matrice.nbcolonne * sizeof (RVB));
  }
//affectation du tableau
  for (i=0;i<matrice.nbligne; i++){
    for (j=0;j< matrice.nbcolonne;j++){
      fscanf(fichier, "%d", &matrice.tabpixel[i][j].rouge);
      fscanf(fichier, "%d", &matrice.tabpixel[i][j].vert);
      fscanf(fichier, "%d", &matrice.tabpixel[i][j].bleu);
    }
  }

//code pour vérifier les valeurs RVB pixels pendant le code
/*
  for (j = 0; j < matrice.nbligne; j++){
    for (i = 0; i < matrice.nbcolonne; i ++){
      printf("%d ",matrice.tabpixel[i][j].rouge) ;
      printf("%d ",matrice.tabpixel[i][j].vert) ;
      printf("%d\n",matrice.tabpixel[i][j].bleu) ;
    }
  }
*/
  fclose(fichier);
  return matrice;
}
void liberer(struct image matrice){
  //libération du tableau
  for (int i=0;i<matrice.nbligne;i++){
    free(matrice.tabpixel[i]);
  }
  free(matrice.tabpixel);
  matrice.tabpixel=NULL;
}



// Auteur :  Khadija
// Date :  24/05
// Modifications: Lisa
// Résumé : Sauvegarde un fichier ppm et vérifie si la sauvegarde a échoué ou pas
// Entrée(s) : Une structure d'image
// Sortie(s) : 0 si la sauvegarde a échoué, 1 si le fichier a bien été sauvegardé
int sauvegarde(struct image matrice, char* nom){
  FILE *fichier = fopen(nom,"w+");// ouverture en mode écriture
  FILE *test;
  int y;
  //header du fichier
  fprintf(fichier,"%c",'P');
  fprintf(fichier,"%d\n",3);
  fprintf(fichier,"%d %d\n",matrice.nbligne, matrice.nbcolonne);
  fprintf(fichier,"%d\n",255);
  //écriture dans le fichier
  for (int i = 0; i < matrice.nbligne; i++){
    for (int j = 0; j < matrice.nbcolonne; j ++){
      fprintf(fichier,"%d\n",matrice.tabpixel[i][j].rouge);
      fprintf(fichier,"%d\n",matrice.tabpixel[i][j].vert);
      fprintf(fichier,"%d\n",matrice.tabpixel[i][j].bleu);
    }
  }
  //test de sauvegarde du fichier
  fclose(fichier);
  test =fopen(nom,"r"); //ouverture en mode lecture
  if(test==NULL){
    fclose(test);
    return 0; //la sauvegarde a échoué
  }else{
    fclose(test);
    return 1; //la sauvegarde est un succès
  }
}

// Auteur :  Lisa
// Date :  26/05
// Modifications : Khadija
// Résumé :  Procédure pour créer une image croix
// Entrée(s) :  nom de l'image, largeur, hauteur et epaisseur de la croix
// Sortie(s) :  -
void creation(char* nom, int x, int y, int epaisseur){
  FILE *fichier = fopen(nom,"w+");// ouverture en mode écriture
  struct image matrice;
  //écriture pour le fichier ppm
  fprintf(fichier,"%c",'P');
  fprintf(fichier,"%d\n",3);
  fprintf(fichier,"%d %d\n",x, y);
  fprintf(fichier,"%d\n",255);
  for (int i = 0; i < y; i++){
    for (int j = 0; j < x; j ++){
      //écriture dans le fichierdes pixels
      if((((x/2)-epaisseur)<j)&&(((x/2)+epaisseur)>j)){
        fprintf(fichier,"%d\n%d\n%d\n",0,0,0);
      }else if((((x/2)-epaisseur)<i)&&(((x/2)+epaisseur)>i)){
      fprintf(fichier,"%d\n%d\n%d\n",0,0,0);
    }else{
      fprintf(fichier,"%d\n%d\n%d\n",255,255,255);
    }
  }
  }
}


// Auteur :  Lisa
// Date :  24/05
// Modifications : Khadija
// Résumé :  Procédure pour transformer les RVB pixels en gris
// Entrée(s) :  Variable struct d'une image
// Sortie(s) :  -
void gris(struct image matrice){
  int y;
  for (int i=0;i<matrice.nbligne;i++){
    for(int j=0;j<matrice.nbcolonne;j++){
      //formule pour gris
      y = (matrice.tabpixel[i][j].rouge*0.2126+matrice.tabpixel[i][j].vert*0.07152+matrice.tabpixel[i][j].bleu*0.00722)*3;
      matrice.tabpixel[i][j].rouge = y;
      matrice.tabpixel[i][j].vert = y;
      matrice.tabpixel[i][j].bleu = y;
    }
  }
}


//Auteur :  Lisa
//Date :  25/05
// Modifications : Khadija
//Résumé :  Procédure pour transformer les RVB pixels en noir et blanc à un certain seuil
//Entrée(s) :  Variable struct d'une image
//Sortie(s) :  -
void binarisation(struct image matrice, int seuil){
  int score;
  for (int i=0;i<matrice.nbligne;i++){
    for(int j=0;j<matrice.nbcolonne;j++){
      //formule pour la binarisation
      score = (matrice.tabpixel[i][j].rouge + matrice.tabpixel[i][j].vert + matrice.tabpixel[i][j].bleu)/2.75;
      if (seuil>score){
        //pixel noir
        matrice.tabpixel[i][j].rouge = 0;
        matrice.tabpixel[i][j].vert = 0;
        matrice.tabpixel[i][j].bleu = 0;
      }else{
        //pixel blanc
        matrice.tabpixel[i][j].rouge = 255;
        matrice.tabpixel[i][j].vert = 255;
        matrice.tabpixel[i][j].bleu = 255;
      }
    }
  }
}

// Auteur :  Lisa
// Date :  24/05
// Modifications : Khadija
// Résumé :  Procédure pour transformer l'image en miroir'
// Entrée(s) :  Variable struct d'une image
// Sortie(s) :  -
void miroir(struct image matrice){
  struct image matricestock;
  matricestock.tabpixel=malloc(matrice.nbligne * sizeof (RVB*));
  matricestock.nbcolonne = matrice.nbcolonne;
  matricestock.nbligne = matrice.nbligne;
  //allocation tableau dynamique 2D pour stocker les valeurs RVB pixels
  for (int i=0;i<matrice.nbligne;i++){
    matricestock.tabpixel[i]=malloc(matrice.nbcolonne * sizeof (RVB));
  }
  //affectation du tableau
  for (int i=0;i<matrice.nbligne; i++){
    for (int j=0;j< matrice.nbcolonne;j++){
      matricestock.tabpixel[i][j].rouge =matrice.tabpixel[i][j].rouge;
      matricestock.tabpixel[i][j].vert =matrice.tabpixel[i][j].vert;
      matricestock.tabpixel[i][j].bleu =matrice.tabpixel[i][j].bleu;
    }
  }
  //mirroir
  for(int i=0;i<matrice.nbligne;i++){
    for(int j=0;j<matrice.nbcolonne;j++){
      matrice.tabpixel[i][j].rouge = matricestock.tabpixel[i][(matricestock.nbcolonne)-(j+1)].rouge;
      matrice.tabpixel[i][j].vert = matricestock.tabpixel[i][(matricestock.nbcolonne)-(j+1)].vert;
      matrice.tabpixel[i][j].bleu = matricestock.tabpixel[i][(matricestock.nbcolonne)-(j+1)].bleu;
    }
  }
  //libération du tableau
    for (int i=0;i<matricestock.nbligne;i++){
      free(matricestock.tabpixel[i]);
    }
    free(matricestock.tabpixel);
    matricestock.tabpixel=NULL;
}

// Auteur :  Lisa
// Date :  25/05
// Modifications : Khadija
// Résumé :  Procédure pour rotationner l'image
// Entrée(s) :  Variable struct d'une image
// Sortie(s) :  -
void rotation(struct image matrice){
  struct image matricestock;
  matricestock.tabpixel=malloc(matrice.nbligne * sizeof (RVB*));
  matricestock.nbcolonne = matrice.nbcolonne;
  matricestock.nbligne = matrice.nbligne;
  //allocation tableau dynamique 2D pour stocker les valeurs RVB pixels
  for (int i=0;i<matrice.nbligne;i++){
    matricestock.tabpixel[i]=malloc(matrice.nbcolonne * sizeof (RVB));
  }
  //affectation du tableau
  for (int i=0;i<matrice.nbligne; i++){
    for (int j=0;j< matrice.nbcolonne;j++){
      matricestock.tabpixel[i][j].rouge =matrice.tabpixel[i][j].rouge;
      matricestock.tabpixel[i][j].vert =matrice.tabpixel[i][j].vert;
      matricestock.tabpixel[i][j].bleu =matrice.tabpixel[i][j].bleu;
    }
  }
  //rotation
  for(int i=0;i<matrice.nbcolonne;i++){
    for(int j=0;j<matrice.nbligne;j++){
      matrice.tabpixel[i][j].rouge = matricestock.tabpixel[j][i].rouge;
      matrice.tabpixel[i][j].vert = matricestock.tabpixel[j][i].vert;
      matrice.tabpixel[i][j].bleu = matricestock.tabpixel[j][i].bleu;
    }
  }
  miroir (matrice);
  //libération du tableau
    for (int i=0;i<matricestock.nbligne;i++){
      free(matricestock.tabpixel[i]);
    }
    free(matricestock.tabpixel);
    matricestock.tabpixel=NULL;
}

// Auteur :  Lisa
// Date :  25/05
// Modifications : Khadija
// Résumé :  Procédure pour transformer les RVB pixels négatifs
// Entrée(s) :  Variable struct d'une image
// Sortie(s) :  -
void negatif(struct image matrice){
  for(int i=0;i<matrice.nbligne;i++){
    for(int j=0;j<matrice.nbcolonne;j++){
      matrice.tabpixel[i][j].rouge = 255 - matrice.tabpixel[i][j].rouge;
      matrice.tabpixel[i][j].vert = 255 - matrice.tabpixel[i][j].vert;
      matrice.tabpixel[i][j].bleu = 255 - matrice.tabpixel[i][j].bleu;
    }
  }
}

// Auteur :  Khadija
// Date :  26/05
// Résumé :  Fonction qui permet de créer un histogramme sur la luminance d'une image
// Entrée(s) :  Une structure d'image
// Sortie(s) :  Un tableau d'entier représentant l'histogramme
int *histogramme (struct image matrice){
  int* occurence;
  occurence=NULL;
  occurence=malloc(256*sizeof(int));
  int Histo[256];
  for(int i=0;i<256;i++){
    Histo[i]=0;
  }
  for(int i=0;i<matrice.nbligne;i++){
    for(int j=0;j<matrice.nbcolonne;j++){
       Histo[matrice.tabpixel[i][j].rouge]++;
     }
     for (int h=0; h<256;h++){
        occurence[h]=0;
      }
    }
    free(occurence);
    //test de la luminance
    for(int i=0;i<256;i++){
    occurence[i] = 0;
    occurence[i] = Histo[i];
    }
     /*for (int i=0; i<256;i++){
    printf("************ %d a **************: %d\n",i,Histo[i]);
  }*/
  return occurence;
}

// Auteur :  Khadija
// Date :  26/05
// Modifications : Lisa, Nour
// Résumé :  Procédure qui effectue l'opération de convolution
// Entrée(s) :  Une structure d'image et une matrice de convolution 3*3
// Sortie(s) :  -
void convolution(struct image matrice,  float convo[3][3]){
  struct image matrice1;
  matrice1=stockcadre(matrice,1);
  int somme1,somme2,somme3;
  for (int i=1;i<matrice1.nbligne-1;i++){
    for(int j=1;j<matrice1.nbcolonne-1;j++){
      somme1=0;
      somme2=0;
      somme3=0;
      somme1=convo[0][0]*matrice1.tabpixel[i-1][j-1].rouge+convo[0][1]*matrice1.tabpixel[i][j-1].rouge+convo[0][2]*matrice1.tabpixel[i+1][j-1].rouge+convo[1][0]*matrice1.tabpixel[i-1][j].rouge+convo[1][1]*matrice1.tabpixel[i][j].rouge+convo[1][2]*matrice1.tabpixel[i+1][j].rouge+convo[2][0]*matrice1.tabpixel[i-1][j+1].rouge+convo[2][1]*matrice1.tabpixel[i][j+1].rouge+convo[2][2]*matrice1.tabpixel[i+1][j+1].rouge;
      somme2=convo[0][0]*matrice1.tabpixel[i-1][j-1].vert+convo[0][1]*matrice1.tabpixel[i][j-1].vert+convo[0][2]*matrice1.tabpixel[i+1][j-1].vert+convo[1][0]*matrice1.tabpixel[i-1][j].vert+convo[1][1]*matrice1.tabpixel[i][j].vert+convo[1][2]*matrice1.tabpixel[i+1][j].vert+convo[2][0]*matrice1.tabpixel[i-1][j+1].vert+convo[2][1]*matrice1.tabpixel[i][j+1].vert+convo[2][2]*matrice1.tabpixel[i+1][j+1].vert;
      somme3=convo[0][0]*matrice1.tabpixel[i-1][j-1].bleu+convo[0][1]*matrice1.tabpixel[i][j-1].bleu+convo[0][2]*matrice1.tabpixel[i+1][j-1].bleu+convo[1][0]*matrice1.tabpixel[i-1][j].bleu+convo[1][1]*matrice1.tabpixel[i][j].bleu+convo[1][2]*matrice1.tabpixel[i+1][j].bleu+convo[2][0]*matrice1.tabpixel[i-1][j+1].bleu+convo[2][1]*matrice1.tabpixel[i][j+1].bleu+convo[2][2]*matrice1.tabpixel[i+1][j+1].bleu;
      if (somme1<=0){
        somme1=0;
      }
      if (somme2<=0){
        somme2=0;
      }
      if (somme3<=0){
        somme3=0;
      }
      matrice.tabpixel[i-1][j-1].rouge=somme1;
      matrice.tabpixel[i-1][j-1].vert=somme2;
      matrice.tabpixel[i-1][j-1].bleu=somme3;
    }
  }
}

// Auteur :  Khadija
// Date :  26/05
// Résumé :  Procédure qui permet de corriger un histogramme mal reparti sur l'ensemble de ses composantes
// Entrée(s) :  Une structure d'image
// Sortie(s) :  -
void recadragedynamique(struct image matrice){
  gris(matrice);
  int nbmin, nbmax, min, max;
  int x;
  nbmin=100;
  nbmax=0;
  min=255;
  max=0;
  int* histo;
  histo=NULL;
  histo=malloc(256*sizeof(int));
  histo=histogramme(matrice);
  for (int h=0;h<256;h++){
    if (histo[h]>nbmax){
      nbmax=histo[h];
      max=h;
    }
    if (histo[h]<nbmin){
      nbmin=histo[h];
      min=h;
    }
  }
  x=255/(max-min);
  for (int i=0;i<matrice.nbligne;i++){
    for(int j=0;j<matrice.nbcolonne;j++){
      matrice.tabpixel[i][j].rouge = (matrice.tabpixel[i][j].rouge-min)*x;
      matrice.tabpixel[i][j].vert = (matrice.tabpixel[i][j].vert-min)*x;
      matrice.tabpixel[i][j].bleu = (matrice.tabpixel[i][j].bleu-min)*x;
    }
  }
}

// Auteur :  Khadija
// Date :  27/05
// Modifications : Lisa
// Résumé :  Fonction qui permet de créer une matrice temporaire qui possède des bords avec tous les canaux des pixels à 0
// Entrée(s) :  Une structure d'image
// Sortie(s) :  Une structure d'image avec des bords à 0
struct image stockcadre(struct image matrice,int rajout){
struct image matrice1;
matrice1.nbligne=matrice.nbligne+rajout*2;
matrice1.nbcolonne=matrice.nbcolonne+rajout*2;
matrice1.tabpixel=NULL;
matrice1.tabpixel=malloc((matrice1.nbligne) * sizeof (RVB*));
  for (int i=0;i<matrice1.nbligne;i++){
    matrice1.tabpixel[i]=malloc((matrice1.nbcolonne) * sizeof (RVB));
  }
  for (int i=0;i<matrice1.nbligne; i++){
    for (int j=0;j< matrice1.nbcolonne;j++){
      matrice1.tabpixel[i][j].rouge =0;
      matrice1.tabpixel[i][j].vert =0;
      matrice1.tabpixel[i][j].bleu =0;
      }
  }
  for (int x=rajout;x<matrice.nbligne+rajout; x++){
    for (int y=rajout;y< matrice.nbcolonne+rajout;y++){
      matrice1.tabpixel[x][y].rouge =matrice.tabpixel[x-rajout][y-rajout].rouge;
      matrice1.tabpixel[x][y].vert =matrice.tabpixel[x-rajout][y-rajout].vert;
      matrice1.tabpixel[x][y].bleu =matrice.tabpixel[x-rajout][y-rajout].bleu;
    }
  }
  return matrice1;
  }


// Auteur :  Lisa
// Date :  28/05
// Résumé :  Procédure qui permet de créer un effet de dilatation
// Entrée(s) : Structure d'image et structure de croix
// Sortie(s) :  -
void croixblanche(struct image croix){
  for(int i=0;i<croix.nbligne;i++){
    for(int j=0;j<croix.nbcolonne;j++){
      if(croix.tabpixel[i][j].rouge==255){
        croix.tabpixel[i][j].rouge=0;
        croix.tabpixel[i][j].vert=0;
        croix.tabpixel[i][j].bleu=0;
      }else{
        croix.tabpixel[i][j].rouge=255;
        croix.tabpixel[i][j].vert=255;
        croix.tabpixel[i][j].bleu=255;
      }
    }
  }
}

// Auteur :  Khadija
// Date :  28/05
// Modifications : Lisa
// Résumé :  Procédure qui permet de créer un effet de dilatation
// Entrée(s) : Structure d'image et structure de croix
// Sortie(s) :  -
void erosion(struct image matrice, struct image croix){
  gris(matrice);
  struct image matrice1;
  matrice1 = stockcadre(matrice,2);
  int min;
  for (int i=2;i<matrice1.nbligne-2;i++){
    for(int j=2;j<matrice1.nbcolonne-2;j++){
      min = 255;
      for (int x=0;x<3;x++){
        for(int y=0;y<3;y++){
          if ((matrice1.tabpixel[i+x][j+y].rouge<min)&&(croix.tabpixel[x+2][y+2].rouge!=0)){
            min=matrice1.tabpixel[i+x][j+y].rouge;
          }
        }
      }
      matrice.tabpixel[i-2][j-2].rouge=min;
      matrice.tabpixel[i-2][j-2].vert=min;
      matrice.tabpixel[i-2][j-2].bleu=min;
    }
  }
}

// Auteur :  Khadija
// Date :  28/05
// Modifications : Lisa
// Résumé :  Procédure qui permet de créer un effet de dilatation
// Entrée(s) : Structure d'image et structure de croix
// Sortie(s) :  -
void dilatation(struct image matrice, struct image croix){
  gris(matrice);
  struct image matrice1;
  matrice1 = stockcadre(matrice,2);
  int max;
  for (int i=2;i<matrice1.nbligne-2;i++){
    for(int j=2;j<matrice1.nbcolonne-2;j++){
      max = 0;
      for (int x=0;x<3;x++){
        for(int y=0;y<3;y++){
          if ((matrice1.tabpixel[i+x][j+y].rouge>max) && (croix.tabpixel[x+2][y+2].rouge !=0)){
            max=matrice1.tabpixel[i+x][j+y].rouge;
          }
        }
      }
      matrice.tabpixel[i-2][j-2].rouge=max;
      matrice.tabpixel[i-2][j-2].vert=max;
      matrice.tabpixel[i-2][j-2].bleu=max;
    }
  }
}


// Auteur :  Khadija
// Date :  27/05
// Résumé :  Procédure qui effectue le produit de convolution d'une image avec une matrice pour un effet de contraste
// Entrée(s) : Structure d'image
// Sortie(s) : -
void contraste (struct image matrice){
float convo[3][3];
  convo[0][0]=0;
  convo[0][1]=-1;
  convo[0][2]=0;
  convo[1][0]=-1;
  convo[1][1]=5;
  convo[1][2]=-1;
  convo[2][0]=0;
  convo[2][1]=-1;
  convo[2][2]=0;
convolution (matrice,convo);
}

// Auteur :  Khadija
// Date :  27/05
// Résumé :  Procédure qui effectue le produit de convolution d'une image avec une matrice pour un effet de floutage
// Entrée(s) : Structure d'image
// Sortie(s) :  -
void floutage (struct image matrice){
  float convo[3][3];
  convo[0][0]=0.0625;
  convo[1][0]=0.125;
  convo[2][0]=0.0625;
  convo[0][1]=0.125;
  convo[0][2]=0.0625;
  convo[1][2]=0.125;
  convo[1][1]=0.25;
  convo[2][2]=0.0625;
  convo[2][1]=0.125;
  convolution (matrice,convo);
}

// Auteur :  Khadija
// Date :  27/05
// Modifications : Lisa
// Résumé :  Procédure qui effectue le produit de convolution d'une image avec une matrice pour un effet de contour
// Entrée(s) : Structure d'image
// Sortie(s) :  -
void contour (struct image matrice){
  float convo[3][3];
  convo[0][0]=-1;
  convo[1][0]=-1;
  convo[2][0]=-1;
  convo[0][1]=-1;
  convo[0][2]=-1;
  convo[1][2]=-1;
  convo[1][1]=8;
  convo[2][2]=-1;
  convo[2][1]=-1;
  convolution (matrice,convo);
}
