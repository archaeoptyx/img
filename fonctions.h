#ifndef __fonctions_H_
#define __fonctions_H_
//Auteur :  Lisa
//Date :  23/05
//Résumé :  structure pixel avec intensité rouge/vert/bleu
struct RVB {
  int rouge;
  int vert;
  int bleu;
}; typedef struct RVB RVB;
//Auteur :  Lisa
//Date :  23/05
//Modifications: Khadija
//Résumé :  structure image avec nombre de ligne, nombre de colonne, double pointeur de structure pixel et nombre de couleurs
struct image {
  int nbligne;
  int nbcolonne;
  int nbcouleur;
  RVB** tabpixel;
}; typedef struct image image;

struct image Chargement(char* bender);
int sauvegarde(struct image matrice, char* nom);
void creation(char* nom, int x, int y, int epaisseur);
void liberer(struct image matrice);
void gris(struct image matrice);
void binarisation(struct image matrice, int seuil);
void miroir(struct image matrice);
void rotation(struct image matrice);
void negatif(struct image matrice);
void convolution(struct image matrice,float convo[3][3]);
void erosion(struct image matrice, struct image croix);
void dilatation(struct image matrice, struct image croix);
void croixblanche(struct image croix);
void convolution(struct image matrice, float convo[3][3]);
int *histogramme (struct image matrice);
struct image stockcadre(struct image matrice,int rajout);
void recadragedynamique (struct image matrice);
void contraste (struct image matrice);
void floutage (struct image matrice);
void contour (struct image matrice);
#endif
